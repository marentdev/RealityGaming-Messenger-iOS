//
//  LoginViewController.swift
//  RealityGamingMessenger
//
//  Created by Marentdev on 30/08/2017.
//  Copyright © 2017 Marentdev. All rights reserved.
//

import UIKit
import SCLAlertView
import Alamofire
let sessionrg:SessionManager = SessionManager.default
var pseudoRG:String = ""
var _xfToken:String = ""
var _idRG:String = ""
class LoginViewController: UIViewController {

    @IBOutlet weak var password: CustomTextFieldConnexion!
    @IBOutlet weak var username: CustomTextFieldConnexion!
    
    var alert:SCLAlertView = SCLAlertView()
    override func viewDidLoad() {
        super.viewDidLoad()

      self.startNetworkReachabilityObserver()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.username.resignFirstResponder()
        self.password.resignFirstResponder()
    }

    func startNetworkReachabilityObserver() {
        let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "https://realitygaming.fr")
        reachabilityManager?.listener = { status in
            
            switch status {
                
            case .notReachable:
                print("The network is not reachable")
                
            case .unknown :
                print("It is unknown whether the network is reachable")
                
            case .reachable(.ethernetOrWiFi):
                print("The network is reachable over the WiFi connection")
                
            case .reachable(.wwan):
                print("The network is reachable over the WWAN connection")
                
            }
        }
        reachabilityManager?.startListening()
    }
    @IBAction func Connexion(_ sender: UIButton) {
        self.connexionRG(pseudo: username.text!, password: password.text!)
    }
    
    func connexionRG(pseudo:String, password:String) {
        let parametres:Parameters = ["login":pseudo, "register":0, "password":password, "remember":1, "cookie_check":1, "_xfToken":"", "redirect":"https://realitygaming.fr"]
        
        sessionrg.request(URL(string: "https://realitygaming.fr/login/login")!, method: .post, parameters: parametres, encoding: URLEncoding.default, headers: nil).responseString { (responce) in
            let result:String = responce.result.value!
            print(result)
            if result.contains("S'il vous plaît, essayez de nouveau.") || result.contains("demandé n'a pu être trouvé"){
                self.alert.showError("Erreur connexion", subTitle: "Mot de passe ou pseudo incorrect", duration: 3)
            }else if result.contains("taigachat_message") || result.contains("messageShoutbox") {
               /// showNotification(title: "Successful - Login", message: "Vous êtes maintenant connecté a RealityGaming !")
                getTokenRG(reponce: result)
                getid(reponce: result)
                let travaille_userpane = GetElement(input: result.replacingOccurrences(of: "\"", with: "").replacingOccurrences(of: "\n", with: "") as NSString, paternn: "<div class=section visitorPanel>(.*?)</div").replacingOccurrences(of: "</div", with: "")
                pseudoRG = GetElement(input: travaille_userpane as NSString, paternn: "alt=(.*?)>").replacingOccurrences(of: "alt=", with: "").replacingOccurrences(of: "/>", with: "") as String
                self.performSegue(withIdentifier: "Chat", sender: self)
            }else if result.contains("Two-Step Verification Required") || result.contains("La vérification en deux étapes est requise"){
                pseudoRG = GetElement(input: result.replacingOccurrences(of: "\"", with: "").replacingOccurrences(of: "\n", with:"").replacingOccurrences(of: "\t", with: "") as NSString, paternn: "data-redirect=yes><dl class=ctrlUnit><dt>(.*?)textheading>").replacingOccurrences(of: "</dd></dl> <h3 class=textHeading>", with: "").replacingOccurrences(of: "data-redirect=yes><dl class=ctrlUnit><dt>Connexion en tant que:</dt><dd>", with: "").replacingOccurrences(of: "<h3 class=textHeading>", with: "").replacingOccurrences(of: "</dd>", with: "").replacingOccurrences(of: "</dl>", with: "") as String
                self.alert = SCLAlertView()
                self.alert.addButton("Code Email", action: {
                    //Code par Email
                    self.alert = SCLAlertView()
                    let code = self.alert.addTextField("Code")
                    self.alert.addButton("Valider", action: {
                        self.otp(Email: true, Code: "\(code.text)")
                    })
                    self.alert.showSuccess("Verification en deux étapes", subTitle: "")
                })
                self.alert.addButton("Code Authenticator", action: {
                    //Code authenticator
                    self.alert = SCLAlertView()
                    let code = self.alert.addTextField("Code")
                    self.alert.addButton("Valider", action: {
                        self.otp(Email: false, Code: "\(code.text)")
                    })
                    self.alert.showSuccess("Verification en deux étapes", subTitle: "")
                })
                self.alert.showNotice("Verification", subTitle: "Comment recevez vous le code pour le compte \(pseudoRG) ?")
            }else {
               self.alert.showError("Erreur Connexion", subTitle: "Veuillez reassayer ultérieurement. Si l'erreur persiste veuillez contacter -Marent-", duration: 3)
            }
        }
    }
    func otp(Email:Bool, Code:String) {
        var provider:String = "totp"
        if Email {
            provider = "email"
        }
        let parametre:Parameters = ["code":Code,"trust":"1","provider":provider,"_xfConfirm":"1","_xfToken":"","remember":"1", "save":"Confirmer", "_xfRequestUri":"/login/two-step?redirect=https%3A%2F%2Frealitygaming.fr%2F&remember=1", "_xfNoRedirect":1, "redirect":"https://realitygaming.fr/"]
        let url:URL = URL(string: "https://realitygaming.fr/login/two-step")!
        sessionrg.request(url, method: .post, parameters: parametre).responseString { (reponce) in
            let reponcef:String = reponce.result.value!
            print(reponcef)
            if reponcef.contains("taigachat_message") || reponcef.contains("messageShoutbox"){
                getTokenRG(reponce: reponcef)
                getid(reponce: reponcef)
                self.performSegue(withIdentifier: "Chat", sender: self)
            }else {
                //Erreur
                self.alert.dismiss(animated: true, completion: nil)
                self.alert.showError("Erreur Two-Step", subTitle: "Code invalide !", duration: 3)
                _xfToken = ""
            }
        }
    }
}

func getTokenRG(reponce:String){
    _xfToken = GetElement(input: reponce.replacingOccurrences(of: "\"", with: "") as NSString, paternn: "type=hidden name=_xfToken value=(.*?)/").replacingOccurrences(of: "type=hidden name=_xfToken value=", with: "").replacingOccurrences(of: "/", with: "") as String
}

func getid(reponce:String){
    _idRG = GetElement(input: reponce.replacingOccurrences(of: "\"", with: "") as NSString, paternn: "(.*?),").replacingOccurrences(of: "user_id: ", with: "").replacingOccurrences(of: ",", with: "")
    
}
func deconnexionRG()
{
    sessionrg.request(URL(string: "https://realitygaming.fr/")!).responseString { (responce) in
        let result:String = responce.result.value!
        getTokenRG(reponce: result)
        let parametres:Parameters = ["_xfToken":_xfToken]
        sessionrg.request(URL(string: "https://realitygaming.fr/logout")!, method: .get, parameters: parametres, encoding: URLEncoding.default, headers: nil).responseString { (responce) in
            
        }
    }
}
