//
//  ConversationsTableViewCell.swift
//  RealityGamingMessenger
//
//  Created by Marentdev on 30/08/2017.
//  Copyright © 2017 Marentdev. All rights reserved.
//

import UIKit

class ConversationsTableViewCell: UITableViewCell {

    @IBOutlet weak var Titre: UILabel!
    @IBOutlet weak var membres: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
