//
//  ChatViewController.swift
//  RealityGamingMessenger
//
//  Created by Marentdev on 30/08/2017.
//  Copyright © 2017 Marentdev. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import Alamofire
class ChatViewController: JSQMessagesViewController {
    var messages = [JSQMessage]()
    var ready:Bool = false
    lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
    lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()
    var avid:[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize(width: 25, height: 25)
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize(width: 25, height: 25)
        self.senderId = pseudoRG
        self.senderDisplayName = pseudoRG
        // Do any additional setup after loading the view.
        self.navigationItem.title = Clean_string(text: disc_titre)
        self.navigationItem.leftBarButtonItem?.title = "Conversations"
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupPage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setupPage() {
        sessionrg.request(URL(string: "https://realitygaming.fr/conversations/\(id_conv)")!).responseString { (reponce) in
            let result:String = reponce.result.value!.replacingOccurrences(of: "\"", with: "marentisbackinthegame34567").replacingOccurrences(of: "\n", with: "marentthenewswift4535")
            print(result)
            let travail_mp = matches(for: "<li id=marentisbackinthegame34567message-(.*?)<div id=marentisbackinthegame34567likes-message-", in: result)
            print("\n\n\n")
            print(travail_mp[0])
            for i in 0 ..< travail_mp.count {
                let msg = Clean_string(text: GetElement(input: travail_mp[i] as NSString, paternn: "<blockquote class=marentisbackinthegame34567messageText SelectQuoteContainer ugc baseHtmlmarentisbackinthegame34567>(.*?)</blockquote>marentthenewswift4535</article>").replacingOccurrences(of: "<blockquote class=marentisbackinthegame34567messageText SelectQuoteContainer ugc baseHtmlmarentisbackinthegame34567>", with: "").replacingOccurrences(of: "</blockquote>marentthenewswift4535</article>", with: "").replacingOccurrences(of: "marentthenewswift4535", with: "").replacingOccurrences(of: "marentisbackinthegame34567", with: "\"").replacingOccurrences(of: "Cliquez pour agrandir...", with: "\n"))
                let auteur = Clean_string(text: GetElement(input: travail_mp[i] as NSString, paternn: "data-author=marentisbackinthegame34567(.*?)marentisbackinthegame34567>").replacingOccurrences(of: "marentisbackinthegame34567>", with: "").replacingOccurrences(of: "data-author=marentisbackinthegame34567", with: ""))
                self.addMessage(withId: auteur, name: auteur, text: msg)
                let imgid = GetElement(input: travail_mp[i] as NSString, paternn: "class=marentisbackinthegame34567avatar Av(.*?)m").replacingOccurrences(of: "class=marentisbackinthegame34567avatar Av", with: "").replacingOccurrences(of: "m", with: "")
                self.avid.append(imgid)
            }
            self.ready = true
            self.finishReceivingMessage()
        }
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item] // 1
        if message.senderId == senderId { // 2
            return outgoingBubbleImageView
        } else { // 3
            return incomingBubbleImageView
        }
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
    }
    
    private func setupIncomingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }
    
    private func addMessage(withId id: String, name: String, text: String) {
        if let message = JSQMessage(senderId: id, displayName: name, text: text) {
            messages.append(message)
        }
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString? {
        let message = messages[indexPath.item]
        switch message.senderId {
        case senderId:
            return nil
        default:
            guard let senderDisplayName = message.senderDisplayName else {
                assertionFailure()
                return nil
            }
            return NSAttributedString(string: senderDisplayName)
        }
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        let message = messages[indexPath.item]
        if ready {
        if message.senderId == senderId {
            cell.textView?.textColor = UIColor.white
            loadImageFromUrl(url: GetLinkAvtar(id: self.avid[indexPath.row]), view: cell.avatarImageView)
        } else {
            cell.textView?.textColor = UIColor.black
            loadImageFromUrl(url: GetLinkAvtar(id: self.avid[indexPath.row]), view: cell.avatarImageView)
        }
        }
        cell.avatarImageView.layer.masksToBounds = true
        cell.avatarImageView.layer.cornerRadius = 25 / 2
        return cell
    }
    override func didPressAccessoryButton(_ sender: UIButton) {
    }
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        addMessage(withId: senderId, name: pseudoRG, text: text!)
        JSQSystemSoundPlayer.jsq_playMessageSentSound() // 4
        finishSendingMessage() // 5
    }
}
