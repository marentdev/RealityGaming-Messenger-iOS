//
//  ViewController.swift
//  RealityGamingMessenger
//
//  Created by Marentdev on 30/08/2017.
//  Copyright © 2017 Marentdev. All rights reserved.
//

import UIKit
import Alamofire
var id_conv:String = ""
var disc_titre:String = ""
class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableview: UITableView!
    var discussion_titre:[String] = []
    var discussion_membres:[String] = []
    var discussion_avatar:[String] = []
    var discussion_id:[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupPage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupPage() {
        sessionrg.request(URL(string: "https://realitygaming.fr/conversations/")!).responseString { (responce) in
            let result:String = responce.result.value!.replacingOccurrences(of: "\"", with: "").replacingOccurrences(of: "\n", with: "")
           let discussion_titre_work = matches(for: "Sélectionnez la conversation: '(.*?)'", in: result)
            for i in 0 ..< discussion_titre_work.count {
                let clean = discussion_titre_work[i].replacingOccurrences(of: "Sélectionnez la conversation: '", with: "").replacingOccurrences(of: "'", with: "")
                self.discussion_titre.append(clean)
            }
            let discussion_membre_work = matches(for: "title=Commence une conversation>(.*?),<a href=conversations/", in: result)
            for i in 0 ..< discussion_membre_work.count {
                var clean = discussion_membre_work[i].replacingOccurrences(of: "title=Commence une conversation>", with: "").replacingOccurrences(of: ",<a href=conversations/", with: "").replacingOccurrences(of: "<a href=members/", with: "").replacingOccurrences(of: "/ class=username dir=auto>", with: "<<>").replacingOccurrences(of: "</a>", with: "").replacingOccurrences(of: "<a ", with: "").replacingOccurrences(of: "href=fabien", with: "").replacingOccurrences(of: "href=jb", with: "").replacingOccurrences(of: ",", with: ">><")
                let clean_2 = matches(for: ">><(.*?)<<>", in: clean)
                for j in 0 ..< clean_2.count {
                    clean = clean.replacingOccurrences(of: clean_2[j], with: ", ")
                }
                self.discussion_membres.append(clean)
            }
            let discussion_avatar_work = matches(for: "<img src=(.*?)>", in: result)
            for i in 0 ..< discussion_avatar_work.count {
                let clean = GetElement(input: discussion_avatar_work[i] as NSString, paternn: "[0-9]/(.*?)jpg").replacingOccurrences(of: "1/", with: "").replacingOccurrences(of: ".jpg", with: "").replacingOccurrences(of: "9/", with: "").replacingOccurrences(of: "0/", with: "").replacingOccurrences(of: "2/", with: "").replacingOccurrences(of: "4/", with: "").replacingOccurrences(of: "3/", with: "").replacingOccurrences(of: "5/", with: "").replacingOccurrences(of: "6/", with: "").replacingOccurrences(of: "7/", with: "").replacingOccurrences(of: "8/", with: "")
                self.discussion_avatar.append(clean)
                print(clean)
            }
            let discussion_id_work = matches(for: "<li id=conversation(.*?) class=discussionListItem", in: result)
            for i in 0 ..< discussion_id_work.count {
                let clean = discussion_id_work[i].replacingOccurrences(of: "<li id=conversation-", with: "").replacingOccurrences(of: " class=discussionListItem", with: "")
                self.discussion_id.append(clean)
                print(clean)
            }
            self.discussion_avatar.remove(at: 0)
            self.discussion_avatar.remove(at: 0)
            self.discussion_avatar.remove(at: 0)
            print(self.discussion_id)
            self.tableview.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.isHidden = (discussion_titre.count <= 0 ? true : false)
        return discussion_titre.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 94
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "discussions", for: indexPath) as! ConversationsTableViewCell
        if discussion_titre.count > 0 {
            cell.Titre.text = Clean_string(text: discussion_titre[indexPath.row])
            cell.membres.text = Clean_string(text: discussion_membres[indexPath.row])
            if discussion_avatar[indexPath.row] != "" {
                loadImageFromUrl(url: GetLinkAvtar(id: discussion_avatar[indexPath.row]), view: cell.avatar)
            } else {
                cell.avatar.image = #imageLiteral(resourceName: "chat")
            }
            cell.avatar.layer.masksToBounds = true
            cell.avatar.layer.cornerRadius = cell.avatar.frame.width / 2
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        id_conv = discussion_id[indexPath.row]
        disc_titre = discussion_titre[indexPath.row]
        self.performSegue(withIdentifier: "conv", sender: nil)
    }
    

}

extension String {
    func htmlAttributedString() -> NSAttributedString? {
        guard let data = self.data(using: String.Encoding.utf16, allowLossyConversion: false) else { return nil }
        guard let html = try? NSMutableAttributedString(
            data: data,
            options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
            documentAttributes: nil) else { return nil }
        return html
    }
}

func GetLinkAvtar(id:String) -> String {
    var link:String = "https://realitygaming.fr/data/avatars/l/"
    var id1 = ""
    let a:NSString = id as NSString
    if (id.characters.count == 6){
        id1 = a.substring(to: 3)
    }
    else if (id.characters.count == 5){
        id1 = a.substring(to: 2)
    }
    else if (id.characters.count == 4){
        id1 = a.substring(to: 1)
    }
    else{
        id1 = "0"
    }
    link = "https://realitygaming.fr/data/avatars/l/\(id1 as String)/\(id as String).jpg"
    if link == "https://realitygaming.fr/data/avatars/l/0/.jpg"{
        link = "https://realitygaming.fr/styles/realitygaming/xenforo/avatars/avatar_male_l.png"
    }
    return link
}

func Clean_string(text:String) -> String{
    let decodedString = text.htmlAttributedString()?.string
    return decodedString! as String
}

func matches(for regex: String!, in text: String!) -> [String] {
    
    do {
        let regex = try NSRegularExpression(pattern: regex, options: [])
        let nsString = text as NSString
        let results = regex.matches(in: text, range: NSMakeRange(0, nsString.length))
        return results.map { nsString.substring(with: $0.range)}
    } catch let error as NSError {
        print("invalid regex: \(error.localizedDescription)")
        return []
    }
}
func loadImageFromUrl(url: String, view: UIImageView){
    let url = NSURL(string: url)!
    let task =  URLSession.shared.dataTask(with: url as URL) { (responseData, responseUrl, error) -> Void in
        if let data = responseData{
            DispatchQueue.main.async(execute: { () -> Void in
                    view.image = UIImage(data: data)
            })
        }
    }
    task.resume()
}

func GetElement(input:NSString, paternn:NSString) -> NSString {
    let azerty:NSString = input as NSString
    let cowRegex = try! NSRegularExpression(pattern: paternn as String,
                                            options: .caseInsensitive)
    if let cowMatch = cowRegex.firstMatch(in: azerty as String, options: [],
                                          range: NSMakeRange(0, azerty.length)) {
        return (azerty as NSString).substring(with: cowMatch.range) as NSString
        
    }else{
        return ""
    }
}

